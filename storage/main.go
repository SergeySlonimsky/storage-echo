package main

import (
	"storage-echo/storage/config"
	"storage-echo/storage/repository"
)

func main() {
	container := config.BuildContainer()
	ur, err := repository.UserRepository.FindAll()
	err := container.Invoke(func(server *config.Server) {
		server.Run()
	})

	if err != nil {
		panic(err)
	}

}
