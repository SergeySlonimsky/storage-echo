package database

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"os"
	"storage-echo/app/config"
)

func ConnectDatabase(config *config.DbConfig) (*sqlx.DB, error) {
	return sqlx.Open("mysql", getConnectionString(config))
}

func getConnectionString(config *config.DbConfig) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s",
		os.Getenv(config.User),
		os.Getenv(config.Password),
		os.Getenv(config.Host),
		os.Getenv(config.Port),
		os.Getenv(config.Name))
}
