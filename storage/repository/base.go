package repository

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
)

type BaseModel interface {
	TableName() string
}

type BaseRepository struct {
	database *sqlx.DB
	Model    BaseModel
}

func (repository *BaseRepository) FindAll() (*sql.Rows, error) {
	return repository.database.Query(repository.Model.TableName(), "")
}
