package repository

type User struct {
	BaseModel
	Id        int64
	Username  string
	Email     string
	Passsword string
}

func (user *User) TableName() string {
	return "user"
}

type UserRepository struct {
	BaseRepository
	Model *User
}
