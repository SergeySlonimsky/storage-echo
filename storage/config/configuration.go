package config

import (
	"encoding/json"
	"net/http"
	"os"
)

type DbConfig struct {
	User     string
	Password string
	Host     string
	Port     string
	Name     string
}

type ServerConfig struct {
	Port string
}

type Server struct {
	config *ServerConfig
}

func NewServer(config *ServerConfig) *Server {
	return &Server{
		config,
	}
}

func (server *Server) Handler() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/people", findPeople)

	return mux
}

func findPeople(writer http.ResponseWriter, request *http.Request) {
	bytes, _ := json.Marshal("Test")

	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	writer.Write(bytes)

}

func (server *Server) Run() {
	httpServer := &http.Server{
		Addr:    ":" + server.config.Port,
		Handler: server.Handler(),
	}

	httpServer.ListenAndServe()
}

func NewDbConfig() *DbConfig {
	return &DbConfig{
		os.Getenv("MYSQL_USER"),
		os.Getenv("MYSQL_PWD"),
		os.Getenv("MYSQL_HOST"),
		os.Getenv("MYSQL_PORT"),
		os.Getenv("MYSQL_DB"),
	}
}

func NewServerConfig() *ServerConfig {
	return &ServerConfig{
		os.Getenv("SERVER_PORT"),
	}
}
