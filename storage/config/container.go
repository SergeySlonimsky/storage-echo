package config

import (
	"go.uber.org/dig"
	"storage-echo/storage/database"
)

func BuildContainer() *dig.Container {
	container := dig.New()

	container.Provide(NewDbConfig)
	container.Provide(database.ConnectDatabase)
	container.Provide(NewServerConfig)
	container.Provide(NewServer)

	return container

}
