package main

import (
	"os"
	"strconv"
	"time"
)

func main() {
	name := strconv.Itoa(int(time.Now().Unix()))
	os.OpenFile(name+".txt", os.O_RDONLY|os.O_CREATE, 0666)
}
