#!/bin/bash

echo 'Running docker-compose'
cp ./.env.dist ./.env
docker-compose down
docker-compose build
docker-compose up -d
