package config

import "os"

var Configuration Config

type Config struct {
	storageHost string
	storagePort string
}

func Init() {
	loadFromEnv()
}

func (c *Config) GetStorageLink() string {
	return c.storageHost + ":" + c.storagePort
}

func loadFromEnv() {
	Configuration.storageHost = os.Getenv("STORAGE_HOST")
	Configuration.storagePort = os.Getenv("STORAGE_PORT")
}
