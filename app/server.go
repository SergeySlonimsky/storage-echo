package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"storage-echo/app/handler"
)

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		AllowHeaders:     []string{"authorization", "Content-Type"},
		AllowCredentials: true,
		AllowMethods:     []string{echo.OPTIONS, echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	a := e.Group("/auth")
	a.POST("/login", handler.HandleLogin)
	a.POST("/register", handler.HandleRegister)

	r := e.Group("/api", middleware.JWT([]byte("secret")))
	r.GET("/campaigns", handler.HandleGetCampaigns)
	r.GET("/campaigns/:id", handler.HandleGetCampaign)
	r.POST("/campaigns", handler.HandleSetCampaign)
	r.PUT("/campaigns/:id", handler.HandleUpdateCampaign)
	r.DELETE("/campaigns/:id", handler.HandleDeleteCampaign)

	u := e.Group("/user", middleware.JWT([]byte("secret")))
	u.GET("/profile", handler.HandleGetProfile)
	u.GET("/stat", handler.HandleGetUserStat)

	e.Logger.Fatal(e.Start(":1323"))
}
