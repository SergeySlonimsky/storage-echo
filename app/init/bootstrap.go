package init

import (
	"storage-echo/app/config"
	"storage-echo/app/database"
)

func Init() {
	config.LoadConfiguration()
	database.InitDb()
	database.MigrateStructs()
	database.RegisterValidation()
}
