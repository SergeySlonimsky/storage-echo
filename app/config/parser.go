package config

import (
	"encoding/json"
	"log"
	"os"
)

const configPath = "/config/config.json"
const errorName = "Error configuration: "

var Params Config

type DbConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"username"`
	Password string `json:"password"`
	Name     string `json:"name"`
}
type InstaConfig struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}
type Config struct {
	DbConfig    DbConfig    `json:"db"`
	InstaConfig InstaConfig `json:"instagram"`
}

func LoadConfiguration() {
	if &Params != nil {
		dir, err := os.Getwd()
		if err != nil {
			log.Fatal(errorName, err)
		}
		configFile, err := os.Open(dir + configPath)
		defer configFile.Close()
		if err != nil {
			log.Fatal(errorName, err)
		}
		jsonParser := json.NewDecoder(configFile)
		jsonParser.Decode(&Params)
	}
}
