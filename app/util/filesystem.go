package util

import (
	"errors"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

func SaveFromUrl(url string) (path string, err error) {
	response, e := http.Get(url)
	if e != nil {
		log.Println(e)
		return "", err
	}
	defer response.Body.Close()
	dir, err := os.Getwd()
	if err != nil {
		log.Println(err)
		return "", err
	}
	fileMeta := strings.Split(
		strings.Replace(
			strings.Replace(
				response.Header["Content-Type"][0], "[", "", -1), "]", "", -1), "/")
	if fileMeta[0] != "image" {
		return "", errors.New("File " + path + " is not an image")
	}
	path = dir + "/assets/" + time.Now().Format(string(time.Now().UnixNano())) + "." + fileMeta[1]
	file, err := os.Create(path)
	if err != nil {
		log.Println(err)
		return "", err
	}
	_, err = io.Copy(file, response.Body)
	if err != nil {
		log.Println(err)
		return "", err
	}
	file.Close()
	log.Println("FILE PATH: ", path)
	return path, err
}

func DeleteLocalFile(path string) error {
	return os.Remove(path)
}
