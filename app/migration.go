package main

import (
	"golang.org/x/crypto/bcrypt"
	"log"
	"math/rand"
	"storage-echo/app/config"
	"storage-echo/app/database"
	"strconv"
)

var generateHashedPassword string

func main() {
	config.LoadConfiguration()
	database.InitDb()
	hashedPassword, err := hashPassword("test")
	if err != nil {
		log.Panic("PASSWORD WAS NOT GENERATED")
	}
	generateHashedPassword = hashedPassword

	for i := 1; i < 25; i++ {
		user := createDemoUser(i)
		campaigns := rand.Intn(20) + 1
		for j := 1; j < campaigns; j++ {
			campaign := createDemoCampaign(j)
			images := rand.Intn(63) + 1
			for k := 1; k < images; k++ {
				image := createDemoImage(k)
				campaign.Images = append(campaign.Images, image)
			}
			user.Campaigns = append(user.Campaigns, campaign)
		}
		settings := createSettings()
		user.UserSettings = settings
		database.Db.Create(&user)
	}
}

func createDemoUser(counter int) database.User {
	var user database.User
	user.Username = "user" + strconv.Itoa(counter)
	user.Password = generateHashedPassword
	user.Email = user.Username + "@mail.com"
	user.Avatar = "http://poolwallet.com/assets/img/default.jpg"
	return user
}

func createDemoCampaign(counter int) database.Campaign {
	var campaign database.Campaign
	campaign.Name = "Campaign " + strconv.Itoa(counter)
	campaign.Status = "scheduled"
	return campaign
}

func createDemoImage(counter int) database.Image {
	var image database.Image
	image.Url = "http://blg.mktg.com/wp-content/uploads/2014/01/CLiao.jpg"
	image.Text = "Dummy text for image " + strconv.Itoa(counter)
	return image
}

func createSettings() database.UserSettings {
	var settings database.UserSettings
	settings.InstagramLogin = "isitiriss"
	settings.InstagramPassword = "mytestpassword"
	return settings
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}
