package database

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/go-playground/validator.v9"
	"log"
	"time"
)

type User struct {
	BaseModel
	Username     string       `json:"username"`
	Email        string       `json:"email"`
	Password     string       `json:"-"`
	Avatar       string       `json:"avatar"`
	Campaigns    []Campaign   `json:"campaigns" gorm:"foreignkey:UserId"`
	UserSettings UserSettings `json:"user_settings" gorm:"foreignkey:UserId"`
}

func (User) TableName() string {
	return "user"
}

type UserSettings struct {
	Id                uint64 `json:"-" gorm:"primary_key"`
	InstagramLogin    string `json:"instagram_login"`
	InstagramPassword string `json:"instagram_password"`

	UserId uint `json:"user_id"`
}

func (UserSettings) TableName() string {
	return "user_setting"
}

type Login struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type Register struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserValidator struct {
	validator validator.Validate
}

type Statistic struct {
	Campaigns int `json:"campaigns"`
	Images    int `json:"images"`
}

func (uv *UserValidator) Validate(i User) error {
	return uv.validator.Struct(i)
}

func HandleLogin(login string, plainPassword string) (token string, err error) {
	var user User
	err = Db.Where("username = ?", login).Or("email = ?", login).First(&user).Error
	if err != nil {
		log.Println("EEEEEEEEEEEEERROR", err)
	}
	if checkPasswordHash(plainPassword, user.Password) {
		token := jwt.New(jwt.SigningMethodHS256)
		claims := token.Claims.(jwt.MapClaims)
		claims["username"] = user.Username
		claims["user_id"] = user.ID
		claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
		t, err := token.SignedString([]byte("secret"))
		if err != nil {
			logUserError(err)
			return "", err
		}
		return t, nil
	}
	return "", invalidCredentialsError()
}

func HandleRegister(username string, email string, plainPassword string) (user User, err error) {
	user = User{
		Username: username,
		Email:    email,
	}
	pass, err := hashPassword(plainPassword)
	if err != nil {
		return user, err
	}
	user.Password = pass
	log.Println(user)
	if Db.NewRecord(user) {
		Db.Create(&user)
	} else {
		return user, errors.New("user with this username or email already exists")
	}
	return user, err
}

func HandleGetProfile(id int) (user User, err error) {
	err = Db.Where("id =?", id).First(&user).Error
	return user, err
}

func GetUserStatistic(id int) (statistic Statistic, err error) {
	var stat Statistic
	err = Db.Raw("SELECT (SELECT COUNT(*) FROM campaign WHERE user_id=?) as campaigns, (SELECT COUNT(*) FROM image WHERE campaign_id IN (SELECT id FROM campaign WHERE user_id=?)) as images", id, id).Scan(&stat).Error
	return stat, err
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func invalidCredentialsError() error {
	return errors.New("invalid credentials")
}

func logUserError(err error) {
	if err != nil {
		log.Println("ERROR LOGIN: ", err)
	}
}
