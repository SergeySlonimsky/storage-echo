package database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"storage-echo/app/config"
	"time"
)

type BaseModel struct {
	ID        uint64     `json:"id" sql:"AUTO_INCREMENT" gorm:"primary_key,column:id"`
	CreatedAt time.Time  `json:"created_at" gorm:"column:created_at" sql:"DEFAULT:current_timestamp"`
	UpdatedAt time.Time  `json:"updated_at" gorm:"column:updated_at" sql:"DEFAULT:current_timestamp"`
	DeletedAt *time.Time `json:"deleted_at" gorm:"column:deleted_at"`
}

var Db *gorm.DB

func InitDb() {
	dbParams := config.Params.DbConfig
	mysqlConnect := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		dbParams.User, dbParams.Password, dbParams.Host, dbParams.Port, dbParams.Name)
	db, err := gorm.Open("mysql", mysqlConnect)
	db.LogMode(true)
	if err != nil {
		panic("failed to connect database")
	}
	Db = db
}

func MigrateStructs() {
	Db.AutoMigrate(User{})
	Db.AutoMigrate(UserSettings{})
	Db.AutoMigrate(Campaign{})
	Db.AutoMigrate(Image{})
}
