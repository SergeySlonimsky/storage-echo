package database

import (
	"errors"
)

type Campaign struct {
	BaseModel
	Name   string  `json:"name" validate:"required"`
	Status string  `json:"status" validate:"required"`
	UserId uint    `json:"-" validate:"required"`
	Images []Image `json:"images" gorm:"foreignkey:CampaignId"`
}

func (Campaign) TableName() string {
	return "campaign"
}

type Image struct {
	BaseModel
	Url        string `json:"url"`
	Text       string `json:"text"`
	CampaignId uint   `json:"campaign_id"`
}

func (Image) TableName() string {
	return "image"
}

func GetCampaigns(userId int) (campaigns []Campaign, err error) {
	err = Db.Where("user_id = ?", userId).Preload("Images").Find(&campaigns).Error
	return campaigns, err
}

func GetCampaign(userId int, id int) (campaign Campaign, err error) {
	err = Db.Where("user_id=? AND id=?", userId, id).Preload("Images").First(&campaign).Error
	return campaign, err
}

func SetCampaign(campaign Campaign) (result Campaign, err error) {
	if err != nil {
		return campaign, err
	}
	if Db.NewRecord(campaign) {
		err = Db.Create(&campaign).Error
	} else {
		err = errors.New("primary key should be blank")
	}
	return campaign, err
}

func UpdateCampaign(updatable Campaign, userId int, id int) (result Campaign, err error) {
	if (int(updatable.ID) == id) && (int(updatable.UserId) == userId) {
		err = Db.Save(&updatable).Error
	} else {
		err = errors.New("cannot update entity")
	}
	return updatable, err
}

func DeleteCampaign(id int, userId int) error {
	rows := Db.Where("id=? AND user_id=?", id, userId).Delete(Campaign{}).RowsAffected
	if rows == 0 {
		return errors.New("can't delete campaign")
	}
	return nil
}
