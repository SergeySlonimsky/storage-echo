package handler

type HttpError struct {
	Code    int    `json:"error"`
	Message string `json:"message"`
}
