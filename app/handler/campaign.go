package handler

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"log"
	"net/http"
	"storage-echo/app/database"
	"strconv"
)

func HandleGetCampaigns(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(jwt.MapClaims)
	log.Println("CLAIMS", claims)
	userId := int(claims["user_id"].(float64))
	campaigns, err := database.GetCampaigns(userId)
	if err != nil {
		return c.JSON(http.StatusNotFound, HttpError{
			Code:    http.StatusNotFound,
			Message: err.Error(),
		})
	}
	return c.JSON(http.StatusOK, campaigns)
}

func HandleGetCampaign(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(jwt.MapClaims)
	log.Println("CLAIMS", claims)
	userId := int(claims["user_id"].(float64))
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Println(err)
		return c.JSON(http.StatusBadRequest, HttpError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}
	campaigns, err := database.GetCampaign(userId, id)
	if err != nil {
		return c.JSON(http.StatusNotFound, HttpError{
			Code:    http.StatusNotFound,
			Message: err.Error(),
		})
	}
	return c.JSON(http.StatusOK, campaigns)
}

func HandleSetCampaign(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userId := uint(claims["user_id"].(float64))
	var campaign database.Campaign
	err := c.Bind(&campaign)
	campaign.UserId = userId
	if err != nil {
		return c.JSON(http.StatusBadRequest, HttpError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}
	result, err := database.SetCampaign(campaign)
	if err != nil {
		return c.JSON(http.StatusBadRequest, HttpError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}
	return c.JSON(http.StatusOK, result)
}

func HandleUpdateCampaign(c echo.Context) error {
	//TODO	Need refactoring
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userId := int(claims["user_id"].(float64))
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, HttpError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}
	campaign, err := database.GetCampaign(id, userId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, HttpError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}
	c.Bind(&campaign)
	newCampaign, err := database.UpdateCampaign(campaign, userId, id)
	if err != nil {

	}
	return c.JSON(http.StatusOK, newCampaign)
}

func HandleDeleteCampaign(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userId := int(claims["user_id"].(float64))
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, HttpError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}
	err = database.DeleteCampaign(id, userId)
	if err != nil {
		return c.JSON(http.StatusBadRequest, HttpError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}
	return c.JSON(http.StatusOK, HttpError{
		http.StatusOK,
		fmt.Sprintf("campaign %d successfully deleted", id),
	})
}
