package handler

import (
	"github.com/labstack/echo"
	"net/http"
	"storage-echo/app/database"
)

func HandleLogin(c echo.Context) error {
	login := c.FormValue("login")
	plainPassword := c.FormValue("password")
	t, err := database.HandleLogin(login, plainPassword)
	if err != nil {
		return c.JSON(http.StatusBadRequest, HttpError{
			Code:    http.StatusBadRequest,
			Message: err.Error(),
		})
	}
	return c.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}

func HandleRegister(c echo.Context) error {
	username := c.FormValue("username")
	email := c.FormValue("email")
	plainPassword := c.FormValue("password")
	user, err := database.HandleRegister(username, email, plainPassword)
	if &user != nil && err == nil {
		return c.JSON(http.StatusCreated, user)
	}
	return c.JSON(http.StatusBadRequest, HttpError{
		Code:    http.StatusBadRequest,
		Message: err.Error(),
	})
}
