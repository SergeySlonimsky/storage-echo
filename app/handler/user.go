package handler

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"net/http"
	"storage-echo/app/database"
)

func HandleGetProfile(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(jwt.MapClaims)
	userId := int(claims["user_id"].(float64))
	user, err := database.HandleGetProfile(userId)
	if err != nil {
		return c.JSON(http.StatusNotFound, HttpError{
			Code:    http.StatusNotFound,
			Message: err.Error(),
		})
	}
	return c.JSON(http.StatusOK, user)
}

func HandleGetUserStat(c echo.Context) error {
	claims := c.Get("user").(*jwt.Token).Claims.(jwt.MapClaims)
	userId := int(claims["user_id"].(float64))
	statistic, err := database.GetUserStatistic(userId)
	if err != nil {
		return c.JSON(http.StatusNotFound, HttpError{
			Code:    http.StatusNotFound,
			Message: err.Error(),
		})
	}
	return c.JSON(http.StatusOK, statistic)
}
