package main

import (
	"errors"
	"gopkg.in/ahmdrz/goinsta.v1"
	"log"
	"storage-echo/app/config"
	"storage-echo/app/util"
)

func main() {

}

type InstInst struct {
	login    string
	password string
	conn     *goinsta.Instagram
}

func NewInstInst() (*InstInst, error) {
	log.Println(config.Params)
	conn := goinsta.New(config.Params.InstaConfig.Login, config.Params.InstaConfig.Password)
	if err := conn.Login(); err != nil {
		log.Println("INSTA ERRROR: ", err)
		return nil, err
	}
	return &InstInst{
		login:    config.Params.InstaConfig.Login,
		password: config.Params.InstaConfig.Password,
		conn:     conn,
	}, nil
}

func (in *InstInst) Close() {
	in.conn.Logout()
}

func (in *InstInst) UploadPicture(imageUrl, message string) error {
	localPath, err := util.SaveFromUrl(imageUrl)
	if err != nil {
		log.Println(err)
	}
	resp, err := in.conn.UploadPhoto(localPath, message, in.conn.NewUploadID(), 87, 0)
	if resp.Status != "ok" {
		errors.New(err.Error() + errors.New("Can't upload image to instagram. Response Status: "+resp.Status).Error())
	}
	return err

	defer util.DeleteLocalFile(localPath)
}
